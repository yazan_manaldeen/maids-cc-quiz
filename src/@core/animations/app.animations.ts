import {fadeIn, fadeOut} from "./fade";
import {zoomIn, zoomOut} from "./zoom";


export const appAnimations = [
  fadeIn,
  fadeOut,
  zoomIn,
  zoomOut
];
