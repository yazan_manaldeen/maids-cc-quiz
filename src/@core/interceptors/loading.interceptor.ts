import {HttpEvent, HttpHandlerFn, HttpRequest} from '@angular/common/http';
import {inject} from '@angular/core';
import {finalize, Observable, take} from 'rxjs';
import {AppLoadingService} from "../services/loading.service";

export const appLoadingInterceptor = (req: HttpRequest<unknown>, next: HttpHandlerFn): Observable<HttpEvent<unknown>> => {
  const appLoadingService = inject(AppLoadingService);
  let handleRequestsAutomatically = false;

  appLoadingService.auto$
    .pipe(take(1))
    .subscribe((value) => {
      handleRequestsAutomatically = value;
    });

  if (!handleRequestsAutomatically) {
    return next(req);
  }

  appLoadingService._setLoadingStatus(true, req.url);

  return next(req).pipe(
    finalize(() => {
      appLoadingService._setLoadingStatus(false, req.url);
    }));
};
