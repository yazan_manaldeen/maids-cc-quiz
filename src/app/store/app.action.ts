export class GetUsers {
  static readonly type = '[App] Get Users';

  constructor(public page: number) {
  }
}

export class GetUserById {
  static readonly type = '[App] Get User By Id';

  constructor(public userId: number) {
  }
}
