import {Injectable} from "@angular/core";
import {Action, Selector, State, StateContext} from "@ngxs/store";
import {UserModel} from "../models/user.model";
import {GetUserById, GetUsers} from "./app.action";
import {UsersService} from "../services/users.service";
import {tap} from "rxjs";
import {PaginationModel} from "../models/pagination.model";
import {Router} from "@angular/router";


interface AppStateModel {
  pending: boolean;
  users: { [key: string]: UserModel[] };
  selectedUser: UserModel | null;
  pagination: PaginationModel | null
}

const defaults: AppStateModel = {
  pending: true,
  users: {},
  selectedUser: null,
  pagination: null
}

@State({
  name: "appState",
  defaults
})

@Injectable()
export class AppState {

  constructor(
    private _router: Router,
    private _usersService: UsersService
  ) {
  }

  @Selector()
  static pending(state: AppStateModel): boolean {
    return state.pending;
  }

  @Selector()
  static users(state: AppStateModel): (page: number) => UserModel[] | undefined {
    return (page: number): UserModel[] | undefined => {
      return state.users[page];
    };
  }

  @Selector()
  static pagination(state: AppStateModel): PaginationModel | null {
    return state.pagination;
  }

  @Selector()
  static selectedUser(state: AppStateModel): UserModel | null {
    return state.selectedUser;
  }

  @Action(GetUsers)
  getUsers({getState, patchState}: StateContext<AppStateModel>, {page}: GetUsers) {
    patchState({pending: true});
    if (getState().users[page]) {
      patchState({
        pagination: {...getState().pagination, page: page},
        pending: false
      });
      return null;
    }
    return this._usersService.getUsers(page).pipe(
      tap(res => {
        const newObj = {};
        newObj[`${page}`] = res['data'] as UserModel[];
        patchState({
          users: {...getState().users, ...newObj},
          pagination: {
            per_page: res['per_page'],
            total: res['total'],
            total_pages: res['total_pages'],
            page: res['page'] - 1,
          },
          pending: false
        });
        if (getState().pagination.page > getState().pagination.total_pages - 1) {
          this._router.navigate(['app-users', 0]);
        }
      })
    );
  }

  @Action(GetUserById)
  getUserById({patchState}: StateContext<AppStateModel>, {userId}: GetUserById) {
    patchState({pending: true});
    return this._usersService.getUserById(userId).pipe(
      tap(res => {
        patchState({
          selectedUser: res['data'] as UserModel,
          pending: false
        });
      })
    );
  }
}
