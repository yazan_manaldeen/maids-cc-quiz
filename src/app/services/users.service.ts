import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  readonly #serverUrl: string = 'https://reqres.in/api';

  constructor(private _httpClient: HttpClient) {
  }

  getUsers(page: number) {
    return this._httpClient.get(`${this.#serverUrl}/users`, {params: {page: page + 1}});
  }

  getUserById(userId: number) {
    return this._httpClient.get(`${this.#serverUrl}/users/${userId}`);
  }
}
