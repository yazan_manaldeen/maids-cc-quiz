import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AppUsersComponent} from "./components/app-users/app-users.component";
import {UserDetailsComponent} from "./components/user-details/user-details.component";

const routes: Routes = [
  {
    path: 'app-users/:page',
    component: AppUsersComponent,
    title: 'Application Users'
  },
  {
    path: 'user-details/:userId',
    component: UserDetailsComponent,
    title: 'User Details'
  },
  {
    path: '',
    redirectTo: 'app-users/0',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: 'app-users/0',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
