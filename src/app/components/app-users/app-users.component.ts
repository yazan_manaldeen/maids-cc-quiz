import {AfterViewInit, Component, OnDestroy, ViewChild} from '@angular/core';
import {Store} from "@ngxs/store";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {GetUsers} from "../../store/app.action";
import {Observable, Subject, takeUntil} from "rxjs";
import {UserModel} from "../../models/user.model";
import {AppState} from "../../store/app.state";
import {MatTableDataSource} from "@angular/material/table";
import {MatSort} from "@angular/material/sort";
import {PaginationModel} from "../../models/pagination.model";
import {PageEvent} from "@angular/material/paginator";
import {appAnimations} from "../../../@core/animations/app.animations";

@Component({
  selector: 'app-users',
  templateUrl: './app-users.component.html',
  styleUrl: './app-users.component.scss',
  animations: appAnimations
})
export class AppUsersComponent implements AfterViewInit, OnDestroy {
  @ViewChild('usersTable', {read: MatSort}) usersTableMatSort: MatSort;
  searchInputValue: string = '';
  displayedColumns: string[] = ['email', 'first_name', 'last_name'];
  usersDataSource: MatTableDataSource<any> = new MatTableDataSource();
  pending$: Observable<boolean> = this._store.select(AppState.pending);
  pagination$: Observable<PaginationModel> = this._store.select(AppState.pagination);
  private _unsubscribeAll: Subject<any> = new Subject<any>();

  constructor(
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _store: Store
  ) {
    this._activatedRoute.params.pipe(
      takeUntil(this._unsubscribeAll)
    ).subscribe((params: Params) => {
      this._store.dispatch(new GetUsers(+params['page']));
    });

    this._store.select(AppState.pending).pipe(
      takeUntil(this._unsubscribeAll)
    ).subscribe(pending => {
      if (pending) {
        this.usersDataSource.data = [{
          id: this.searchInputValue,
          email: null,
          first_name: null,
          last_name: null,
          avatar: null
        }];
        this.displayedColumns = ['spinner'];
      } else {
        this.usersDataSource.data = this._store.selectSnapshot(AppState.users)(this._store.selectSnapshot(AppState.pagination).page);
        this.displayedColumns = ['email', 'first_name', 'last_name'];
      }
    });
  }

  trackByFn(index: number, item: UserModel): any {
    return item?.id || index;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.searchInputValue = filterValue.trim().toLowerCase();
    this.usersDataSource.filter = this.searchInputValue;
    this.usersDataSource.filterPredicate = (data, filter) => `${data?.id}`.includes(this.searchInputValue)
  }

  changePage(event: PageEvent) {
    this._router.navigate(['app-users', event.pageIndex]);
  }

  clickedRow(row: UserModel) {
    this._router.navigate(['user-details', row.id]);
  }

  ngAfterViewInit() {
    this.usersDataSource.sort = this.usersTableMatSort;
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next(null);
    this._unsubscribeAll.complete();
  }
}
