import {Component, OnDestroy} from '@angular/core';
import {Observable, Subject, takeUntil} from "rxjs";
import {AppState} from "../../store/app.state";
import {Store} from "@ngxs/store";
import {UserModel} from "../../models/user.model";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {GetUserById} from "../../store/app.action";
import {appAnimations} from "../../../@core/animations/app.animations";

@Component({
  selector: 'user-details',
  templateUrl: './user-details.component.html',
  styleUrl: './user-details.component.scss',
  animations: appAnimations
})
export class UserDetailsComponent implements OnDestroy {

  pending$: Observable<boolean> = this._store.select(AppState.pending);
  user$: Observable<UserModel> = this._store.select(AppState.selectedUser);
  private _unsubscribeAll: Subject<any> = new Subject<any>();

  constructor(
    public _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _store: Store
  ) {
    this._activatedRoute.params.pipe(
      takeUntil(this._unsubscribeAll)
    ).subscribe((params: Params) => {
      this._store.dispatch(new GetUserById(+params['userId']));
    });
  }

  get previousPage() {
    return `../../app-users/${this._store.selectSnapshot(AppState.pagination)?.page || 0}`;
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next(null);
    this._unsubscribeAll.complete();
  }
}
