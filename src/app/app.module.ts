import {ENVIRONMENT_INITIALIZER, inject, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {AppUsersComponent} from './components/app-users/app-users.component';
import {UserDetailsComponent} from './components/user-details/user-details.component';
import {HttpClientModule, provideHttpClient, withInterceptors} from "@angular/common/http";
import {TitleStrategy} from "@angular/router";
import {AppLoadingBarComponent} from "../@core/components/loading-bar/loading-bar.component";
import {appLoadingInterceptor} from "../@core/interceptors/loading.interceptor";
import {TitleStrategyService} from "../@core/services/title-strategy.service";
import {NgxsModule} from "@ngxs/store";
import {AppState} from "./store/app.state";
import {
  MatCell,
  MatCellDef,
  MatColumnDef,
  MatFooterCell,
  MatFooterCellDef,
  MatFooterRow,
  MatFooterRowDef,
  MatHeaderCell,
  MatHeaderCellDef,
  MatHeaderRow,
  MatHeaderRowDef,
  MatRow,
  MatRowDef,
  MatTable
} from "@angular/material/table";
import {MatSort, MatSortHeader} from "@angular/material/sort";
import {NgOptimizedImage} from "@angular/common";
import {MatFormField, MatPrefix} from "@angular/material/form-field";
import {MatInput} from "@angular/material/input";
import {MatPaginator} from "@angular/material/paginator";
import {MatProgressSpinner} from "@angular/material/progress-spinner";
import {MatBadge} from "@angular/material/badge";
import {AppLoadingService} from "../@core/services/loading.service";
import {AppSplashScreenService} from "../@core/services/splash-screen.service";
import {MatIcon} from "@angular/material/icon";

@NgModule({
  declarations: [
    AppComponent,
    AppUsersComponent,
    UserDetailsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    NgxsModule.forRoot([AppState]),
    AppLoadingBarComponent,
    MatTable,
    MatColumnDef,
    MatHeaderCell,
    MatCell,
    MatHeaderCellDef,
    MatCellDef,
    MatFooterCell,
    MatFooterCellDef,
    MatHeaderRowDef,
    MatHeaderRow,
    MatRow,
    MatRowDef,
    MatFooterRow,
    MatFooterRowDef,
    NgOptimizedImage,
    MatSort,
    MatSortHeader,
    MatFormField,
    MatInput,
    MatPaginator,
    MatProgressSpinner,
    MatBadge,
    MatIcon,
    MatPrefix
  ],
  providers: [
    provideHttpClient(withInterceptors([appLoadingInterceptor])),
    {provide: ENVIRONMENT_INITIALIZER, useValue: () => inject(AppLoadingService), multi: true,},
    {provide: ENVIRONMENT_INITIALIZER, useValue: () => inject(AppSplashScreenService), multi: true,},
    {provide: TitleStrategy, useClass: TitleStrategyService},
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
