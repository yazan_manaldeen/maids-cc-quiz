export interface PaginationModel {
  page: number;
  per_page: number;
  total_pages: number;
  total: number;
}
